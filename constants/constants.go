package constants

import "github.com/go-redis/redis/v8"

type SentinelOptions struct {
	PrimaryOptions *redis.Options
	ReaderOptions  *redis.RingOptions
}
type KV struct {
	K string
	V interface{}
}
type HKV struct {
	H string
	K string
	V interface{}
}
type SubChan chan *SubMsg
type SubMsg struct {
	Channel      string
	Pattern      string
	Payload      string
	PayloadSlice []string
}
