package v1

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/topebox_packages/mimi-redis/constants"
)

type RedisSentinel struct {
	Primary *redis.Client
	Reader  *redis.Ring
}

var (
	redisClient     *RedisSentinel
	redisClientOnce sync.Once
	bg              = context.Background()
)

func GetRedisClient(opt *constants.SentinelOptions) *RedisSentinel {
	redisClientOnce.Do(func() {
		redisClient = &RedisSentinel{
			Primary: createRedisClient(opt.PrimaryOptions),
			Reader:  createRedisRing(opt.ReaderOptions),
		}
	})
	return redisClient
}
func createRedisClient(opt *redis.Options) (rdc *redis.Client) {
	rdc = redis.NewClient(&redis.Options{
		Addr:     opt.Addr,
		Password: opt.Password,
		DB:       opt.DB,
	})
	if cmd := rdc.Ping(context.Background()); cmd == nil && cmd.Err() != nil {
		log.Println("failed redis connection: ", opt.Addr)
		return
	}
	log.Println("succeed redis connection: ", opt.Addr)
	return
}
func createRedisRing(opt *redis.RingOptions) (rdc *redis.Ring) {
	rdc = redis.NewRing(opt)
	if cmd := rdc.Ping(context.Background()); cmd == nil || cmd.Err() != nil {
		log.Println("failed redis connection: ", opt.Addrs)
		return
	}
	log.Println("succeed redis connection: ", opt.Addrs)
	return
}

// Primary

func (r *RedisSentinel) SetEx(k string, v interface{}, t time.Duration) error {
	return r.Primary.Set(bg, k, v, t).Err()
}
func (r *RedisSentinel) Set(k string, v interface{}) error {
	return r.Primary.Set(bg, k, v, redis.KeepTTL).Err()
}
func (r *RedisSentinel) SAdd(h string, k ...interface{}) error {
	return r.Primary.SAdd(bg, h, k).Err()
}
func (r *RedisSentinel) SRem(h string, k ...interface{}) error {
	return r.Primary.SRem(bg, h, k).Err()
}
func (r *RedisSentinel) HSet(h, k string, v interface{}) error {
	return r.Primary.HSet(bg, h, k, v).Err()
}
func (r *RedisSentinel) HDel(h, k string) error {
	return r.Primary.HDel(bg, h, k).Err()
}
func (r *RedisSentinel) GeoAdd(h, k string, la, lg float64) error {
	g := &redis.GeoLocation{
		Longitude: lg,
		Latitude:  la,
		Name:      k,
	}
	return r.Primary.GeoAdd(bg, h, g).Err()
}
func (r *RedisSentinel) MultiHSet(hkvSlice []*constants.HKV) (err error) {
	_, err = r.Primary.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, hkv := range hkvSlice {
			pipe.HSet(bg, hkv.H, hkv.K, hkv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisSentinel) MultiSAdd(kvSlice []*constants.KV) (err error) {
	_, err = r.Primary.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, kv := range kvSlice {
			pipe.SAdd(bg, kv.K, kv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisSentinel) MultiSRem(kvSlice []*constants.KV) (err error) {
	_, err = r.Primary.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, kv := range kvSlice {
			pipe.SRem(bg, kv.K, kv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisSentinel) Pipelined(f func(pipe redis.Pipeliner) error) (err error) {
	_, err = r.Primary.Pipelined(bg, f)
	return
}
func (r *RedisSentinel) Publish(channel string, msg interface{}) error {
	return r.Primary.Publish(bg, channel, msg).Err()
}
func (r *RedisSentinel) Subscribe(channel string, c constants.SubChan, q chan int) {
	pb := r.Primary.Subscribe(bg, channel)
	defer func() {
		_ = pb.Close()
	}()
	finish := func() {
		for msg := range q {
			if msg == -1 {
				_ = pb.Close()
				return
			}
		}
	}
	go finish()
	for msg := range pb.Channel() {
		if msg != nil && msg.Payload != "" {
			c <- &constants.SubMsg{
				Channel:      msg.Channel,
				Pattern:      msg.Pattern,
				Payload:      msg.Payload,
				PayloadSlice: msg.PayloadSlice,
			}
		}
	}
	return
}

// Reader

func (r *RedisSentinel) Ping() error {
	return r.Reader.Ping(bg).Err()
}
func (r *RedisSentinel) Get(k string) (string, error) {
	return r.Reader.Get(bg, k).Result()
}
func (r *RedisSentinel) SCard(h string) (int64, error) {
	return r.Reader.SCard(bg, h).Result()
}
func (r *RedisSentinel) SIsMember(h string, k interface{}) (bool, error) {
	return r.Reader.SIsMember(bg, h, k).Result()
}
func (r *RedisSentinel) HLen(h string) (int64, error) {
	return r.Reader.HLen(bg, h).Result()
}
func (r *RedisSentinel) HGet(h, k string) (string, error) {
	return r.Reader.HGet(bg, h, k).Result()
}
func (r *RedisSentinel) HGetScan(h, k string, v interface{}) error {
	return r.Reader.HGet(bg, h, k).Scan(v)
}
func (r *RedisSentinel) GeoRadius(h string, la, lg, rad float64) (data []string, err error) {
	q := &redis.GeoRadiusQuery{
		Radius: rad,
		Unit:   "m",
	}
	locations, err := r.Reader.GeoRadius(bg, h, lg, la, q).Result()
	if err != nil {
		return
	}
	for _, location := range locations {
		data = append(data, location.Name)
	}
	return
}
func (r *RedisSentinel) GeoRadiusByMember(h string, k string, rad float64) (data []string, err error) {
	q := &redis.GeoRadiusQuery{
		Radius: rad,
		Unit:   "m",
	}
	locations, err := r.Reader.GeoRadiusByMember(bg, h, k, q).Result()
	if err != nil {
		return
	}
	for _, location := range locations {
		data = append(data, location.Name)
	}
	return
}
func (r *RedisSentinel) MultiGet(sk ...string) (m map[string]string, err error) {
	cmdS, err := r.Reader.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, k := range sk {
			pipe.Get(bg, k)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	m = make(map[string]string)
	for i, cmd := range cmdS {
		m[sk[i]] = cmd.(*redis.StringCmd).Val()
	}
	return
}
func (r *RedisSentinel) MultiHGet(h string, sk ...string) (m map[string]string, err error) {
	cmdS, err := r.Reader.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, k := range sk {
			pipe.HGet(bg, h, k)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	m = make(map[string]string)
	for i, cmd := range cmdS {
		m[sk[i]] = cmd.(*redis.StringCmd).Val()
	}
	return
}
func (r *RedisSentinel) SMembers(k string) ([]string, error) {
	return r.Reader.SMembers(bg, k).Result()
}

func (r *RedisSentinel) GetKeys(pattern string) ([]string, error) {
	return r.Reader.Keys(bg, pattern).Result()
}

func (r *RedisSentinel) GetPrimaryClient() *redis.Client {
	return r.Primary
}

func (r *RedisSentinel) GetReaderClient() *redis.Ring {
	return r.Reader
}

func (r *RedisSentinel) GetClusterClient() *redis.ClusterClient {
	return nil
}
