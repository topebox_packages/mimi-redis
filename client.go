package mimi_redis

import (
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/topebox_packages/mimi-redis/constants"
	v1 "gitlab.com/topebox_packages/mimi-redis/v1"
	v2 "gitlab.com/topebox_packages/mimi-redis/v2"
)

type RedisClientI interface {
	Ping() error
	SetEx(k string, v interface{}, t time.Duration) error
	Set(k string, v interface{}) error
	Get(k string) (string, error)
	SAdd(h string, k ...interface{}) error
	SCard(h string) (int64, error)
	SRem(h string, k ...interface{}) error
	SIsMember(h string, k interface{}) (bool, error)
	SMembers(k string) ([]string, error)
	HLen(h string) (int64, error)
	HSet(h, k string, v interface{}) error
	HGet(h, k string) (string, error)
	HGetScan(h, k string, v interface{}) error
	HDel(h, k string) error
	GeoAdd(h, k string, la, lg float64) error
	GeoRadius(h string, la, lg, rad float64) (data []string, err error)
	GeoRadiusByMember(h string, k string, rad float64) (data []string, err error)
	MultiGet(sk ...string) (m map[string]string, err error)
	MultiHGet(k string, sk ...string) (m map[string]string, err error)
	MultiHSet(hkvSlice []*constants.HKV) (err error)
	MultiSAdd(kvSlice []*constants.KV) (err error)
	MultiSRem(kvSlice []*constants.KV) (err error)
	Pipelined(f func(pipe redis.Pipeliner) error) (err error)
	Publish(channel string, msg interface{}) error
	Subscribe(channel string, c constants.SubChan, q chan int)
	GetKeys(pattern string) ([]string, error)
	GetPrimaryClient() *redis.Client
	GetReaderClient() *redis.Ring
}

func GetSentinel(opt *constants.SentinelOptions) RedisClientI {
	return v1.GetRedisClient(opt)
}
func GetCluster(opt *redis.ClusterOptions) RedisClientI {
	return v2.GetRedisCluster(opt)
}
