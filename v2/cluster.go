package v2

import (
	"context"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/topebox_packages/mimi-redis/constants"
)

type RedisCluster struct {
	Client *redis.ClusterClient
}

var (
	once         sync.Once
	redisCluster *RedisCluster
	bg           = context.Background()
)

func GetRedisCluster(opt *redis.ClusterOptions) *RedisCluster {
	once.Do(func() {
		redisCluster = &RedisCluster{
			Client: redis.NewClusterClient(opt),
		}
	})
	return redisCluster
}

// Primary

func (r *RedisCluster) SetEx(k string, v interface{}, t time.Duration) error {
	return r.Client.Set(bg, k, v, t).Err()
}
func (r *RedisCluster) Set(k string, v interface{}) error {
	return r.Client.Set(bg, k, v, redis.KeepTTL).Err()
}
func (r *RedisCluster) SAdd(h string, k ...interface{}) error {
	return r.Client.SAdd(bg, h, k).Err()
}
func (r *RedisCluster) SRem(h string, k ...interface{}) error {
	return r.Client.SRem(bg, h, k).Err()
}
func (r *RedisCluster) HSet(h, k string, v interface{}) error {
	return r.Client.HSet(bg, h, k, v).Err()
}
func (r *RedisCluster) HDel(h, k string) error {
	return r.Client.HDel(bg, h, k).Err()
}
func (r *RedisCluster) GeoAdd(h, k string, la, lg float64) error {
	g := &redis.GeoLocation{
		Longitude: lg,
		Latitude:  la,
		Name:      k,
	}
	return r.Client.GeoAdd(bg, h, g).Err()
}
func (r *RedisCluster) MultiHSet(hkvSlice []*constants.HKV) (err error) {
	_, err = r.Client.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, hkv := range hkvSlice {
			pipe.HSet(bg, hkv.H, hkv.K, hkv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisCluster) MultiSAdd(kvSlice []*constants.KV) (err error) {
	_, err = r.Client.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, kv := range kvSlice {
			pipe.SAdd(bg, kv.K, kv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisCluster) MultiSRem(kvSlice []*constants.KV) (err error) {
	_, err = r.Client.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, kv := range kvSlice {
			pipe.SRem(bg, kv.K, kv.V)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	return
}
func (r *RedisCluster) Pipelined(f func(pipe redis.Pipeliner) error) (err error) {
	_, err = r.Client.Pipelined(bg, f)
	return
}
func (r *RedisCluster) Publish(channel string, msg interface{}) error {
	return r.Client.Publish(bg, channel, msg).Err()
}
func (r *RedisCluster) Subscribe(channel string, c constants.SubChan, q chan int) {
	pb := r.Client.Subscribe(bg, channel)
	defer func() {
		_ = pb.Close()
	}()
	finish := func() {
		for msg := range q {
			if msg == -1 {
				_ = pb.Close()
				return
			}
		}
	}
	go finish()
	for msg := range pb.Channel() {
		if msg != nil && msg.Payload != "" {
			c <- &constants.SubMsg{
				Channel:      msg.Channel,
				Pattern:      msg.Pattern,
				Payload:      msg.Payload,
				PayloadSlice: msg.PayloadSlice,
			}
		}
	}
	return
}

// Reader

func (r *RedisCluster) Ping() error {
	return r.Client.Ping(bg).Err()
}
func (r *RedisCluster) Get(k string) (string, error) {
	return r.Client.Get(bg, k).Result()
}
func (r *RedisCluster) SCard(h string) (int64, error) {
	return r.Client.SCard(bg, h).Result()
}
func (r *RedisCluster) SIsMember(h string, k interface{}) (bool, error) {
	return r.Client.SIsMember(bg, h, k).Result()
}
func (r *RedisCluster) HLen(h string) (int64, error) {
	return r.Client.HLen(bg, h).Result()
}
func (r *RedisCluster) HGet(h, k string) (string, error) {
	return r.Client.HGet(bg, h, k).Result()
}
func (r *RedisCluster) HGetScan(h, k string, v interface{}) error {
	return r.Client.HGet(bg, h, k).Scan(v)
}
func (r *RedisCluster) GeoRadius(h string, la, lg, rad float64) (data []string, err error) {
	q := &redis.GeoRadiusQuery{
		Radius: rad,
		Unit:   "m",
	}
	locations, err := r.Client.GeoRadius(bg, h, lg, la, q).Result()
	if err != nil {
		return
	}
	for _, location := range locations {
		data = append(data, location.Name)
	}
	return
}
func (r *RedisCluster) GeoRadiusByMember(h string, k string, rad float64) (data []string, err error) {
	q := &redis.GeoRadiusQuery{
		Radius: rad,
		Unit:   "m",
	}
	locations, err := r.Client.GeoRadiusByMember(bg, h, k, q).Result()
	if err != nil {
		return
	}
	for _, location := range locations {
		data = append(data, location.Name)
	}
	return
}
func (r *RedisCluster) MultiGet(sk ...string) (m map[string]string, err error) {
	cmdS, err := r.Client.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, k := range sk {
			pipe.Get(bg, k)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	m = make(map[string]string)
	for i, cmd := range cmdS {
		m[sk[i]] = cmd.(*redis.StringCmd).Val()
	}
	return
}
func (r *RedisCluster) MultiHGet(h string, sk ...string) (m map[string]string, err error) {
	cmdS, err := r.Client.Pipelined(bg, func(pipe redis.Pipeliner) error {
		for _, k := range sk {
			pipe.HGet(bg, h, k)
		}
		return nil
	})
	if err != nil && err != redis.Nil {
		return
	}
	err = nil
	m = make(map[string]string)
	for i, cmd := range cmdS {
		m[sk[i]] = cmd.(*redis.StringCmd).Val()
	}
	return
}
func (r *RedisCluster) SMembers(k string) ([]string, error) {
	return r.Client.SMembers(bg, k).Result()
}

func (r *RedisCluster) GetKeys(pattern string) ([]string, error) {
	return r.Client.Keys(bg, pattern).Result()
}

func (r *RedisCluster) GetPrimaryClient() *redis.Client {
	return nil
}

func (r *RedisCluster) GetReaderClient() *redis.Ring {
	return nil
}

func (r *RedisCluster) GetClusterClient() *redis.ClusterClient {
	return r.Client
}
